#!/bin/sh

set -e


echo "Configure iptables for portspoof"
sed -i "s/#ExecStartPre/ExecStartPre/" /etc/systemd/system/portspoof.service.d/override.conf
sed -i "s/#ExecStartPre/ExecStopPost/" /etc/systemd/system/portspoof.service.d/override.conf


echo "Start portspoof service"
portspoof-start

if ! systemctl is-active -q portspoof; then
    echo "the service portspoof is not active"
    exit 1
fi

echo "Stop portspoof service"
portspoof-stop

if systemctl is-active -q portspoof; then
    echo "the service portspoof is still active"
    exit 1
fi
